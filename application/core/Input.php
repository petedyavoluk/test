<?php

    //дополнительные параметры в адресной строке
    //http://exanche.com/sendmail/index/id/2
    //vars['id'] = 2;
    class Input{

        private static $vars = array();


        public static function setAll($action_parameters){
            self::$vars = $action_parameters;
        }

        public static function setParam($key, $value){
            self::$vars[$key] = $value;
        }

        public static function getAll(){
            return self::$vars;
        }

        public static function getParam($key){
            //if( isset(self::$vars[$key]) && !empty(self::$vars[$key])){
            if( isset(self::$vars[$key])){
                return self::$vars[$key];
            }

            return false;
        }
        public static function isExistParam($key){
            if( isset(self::$vars[$key])){
                return true;
            }

            return false;
        }
    }