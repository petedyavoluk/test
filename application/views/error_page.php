<div class="page-inner">
</div>
<div class="container error-404">
    <h1>Error</h1>
    <h2>Error occurred.</h2>
    <p><?php _o($this->msg); ?></p>
    <p><a href="/index" class="btn red btn-outline">Return home</a></p>
</div>