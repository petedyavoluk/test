<?php

    //
    class Config{

        static private $arr_vars = array();

        static public function Set( $v, $val, $more = '' ){
            if( $more == '' )
                self::$arr_vars[$v] = $val;
            else
                self::$arr_vars[$v][$val] = $more;
        }

        static public function Get( $v ){
            return isset( self::$arr_vars[$v] ) ? self::$arr_vars[$v] : '';
        }
    }