<?php
    ////////////////////////////////////// Зборник вспомогательных функций /////////////////////////////////////////////

    //вызов 404 страницы
    function ErrorPage404(){
        $obj_view = new View('404', 'Page not found!', 'header', 'footer');
        $obj_view->Show();
        exit;
    }

    //ошибка на странице
    function ErrorOnPage($err = ''){
        $obj_view = new View('error_page', 'Error occurred', 'header', 'footer');
        $obj_view->msg = $err;
        $obj_view->Show();
        exit;
    }

    function Success($err = ''){
        $obj_view = new View('success_page', 'Success!', 'header', 'footer');
        $obj_view->msg = $err;
        $obj_view->Show();
        exit;
    }

    //вызов страницы с ошибкой доступа
    function ErrorPermission(){
        $obj_view = new View('error_permission', 'Error Permission!', 'header', 'footer');
        $obj_view->Show();
        exit;
    }

    /**
     * функция для просмора содержимого обькта, массива, переменной
     * @param mixed $obj переменная для вывода
     * @param string $var_dumpp если передан, использовать var_dump для просмотра, вместо print_r
     * @param string $exit если передан, прекратить дальнейшее выполнение кода
     */
    function pre($obj, $var_dumpp = '', $exit = ''){
        echo '<pre>';
        if($var_dumpp == 1){
            var_dump($obj);
        }
        else{
            print_r($obj);
        }
        echo '</pre>';
        if($exit == 1){
            exit;
        }
    }

    //проверка на существование и экранирование
    function _o($param){
        if(isset($param)){
            echo htmlspecialchars($param);
            return true;
        }
        else{
            return false;
        }
    }

    function _e($param){
        return htmlspecialchars($param);
    }

    //
    function _i($key){
        return Input::getParam($key);
    }

    //редирект на переадваемый адресс
    function _Go($url){
        header('Location: ' . $url);
    }

    //формирует url страницы
    function _Link(){
        $arr = func_get_args();
        return SITE_URL . implode('/', $arr);
    }

    //
    function _LinkFromArray($arr){
        return SITE_URL . implode('/', $arr);
    }

    /* проверяет право доступа к странице
        @param array $arr_roles - массив ролей, которым разрешено. Список ролей из user.role ('ADM', 'SSP', 'DSP', 'SA', 'NGINAD' )
    */
    function _CheckPerm(array $arr_roles){
        if(Auth::$_user !== false){
            if(in_array(Auth::$_user['role'], $arr_roles)){
                return true;
            }
        }
        ErrorPermission();
        return false;
    }

    function _isAdmin(){
        if(Auth::$_user !== false){
            if(Auth::$_user['role'] == 'ADM'){
                return true;
            }
        }
        return false;
    }

    //Получение случайного HTML-цвета в 16-м формате
    function random_html_color(){
        return sprintf( '#%02X%02X%02X', rand(0, 255), rand(0, 255), rand(0, 255) );
    }



    function _getExtension($filename) {
        return strtolower(end(explode(".", $filename)));
    }

    function _check_domain($url) {
        $reg = '/^(([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))\.([a-zA-Z]{2,6}|[a-zA-Z0-9-]{2,30}\.[a-zA-Z]{2,3})$/';
        $a = preg_match($reg, $url);
        if($a!=1){return false; }
        return true;
    }

    function _check_domain_rus($url) {
        $reg = "~^(?:(?:https?|ftp|telnet)://(?:[а-яёa-z0-9_-]{1,32}(?::[а-яёa-z0-9_-]{1,32})?@)?)?(?:(?:[а-яёa-z0-9-]{1,128}\.)+(?:ru|su|com|net|org|mil|edu|arpa|gov|biz|info|aero|inc|name|рф|[a-z]{2})|(?!0)(?:(?!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:/[а-яёa-z0-9.,_@%&?+=\~/-]*)?(?:#[^ '\"&]*)?$~i";
        $a = preg_match($reg, $url);
        if($a!=1){return false; }
        return true;
    }
/*
    function _send_mail($to, $subject, $text, $from='no-reply@smartyads.com', $from_name='No-Reply') {
        require_once 'core'.DIRECTORY_SEPARATOR.'extra'.DIRECTORY_SEPARATOR.'PHPMailer-master'.DIRECTORY_SEPARATOR.'PHPMailerAutoload.php';
        $mail_settings = Config::Get('mail_inc');
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $mail_settings['Host'];                 // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $mail_settings['Username'];         // SMTP username
        $mail->Password = $mail_settings['Password'];         // SMTP password
        $mail->SMTPSecure = $mail_settings['SMTPSecure'];     // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $mail_settings['Port'];                 // TCP port to connect to

        $mail->From = $from;
        $mail->FromName = $from_name;

        if(is_array($to)){
            foreach($to as $t){
                $mail->addAddress($t);
            }
        }else{
            $mail->addAddress($to);
        }


        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $text;
        //$mail->AltBody = 'sorry';

        if(!$mail->send()) {
            return 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            return true;
        }
    }
*/

    function get_extension($filename) {
        $path_info = pathinfo($filename);
        if (isset($path_info['extension'])){
            return strtolower($path_info['extension']);
        }else{
            return '';
        }
    }
    function csv_to_array($filename = '', $delimiter = ','){

        if(!file_exists($filename) || !is_readable($filename)){
            return FALSE;
        }
        $header = NULL;
        $data = array();
        if(($handle = fopen($filename, 'r')) !== FALSE){
            while(($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE){
                $pos = strpos($row[0], 'This program cannot be run in DOS mode');
                if($pos == 78){
                    return false;
                }
                if(!$header) $header = $row;
                else{
                    @$data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
        return $data;
    }

    function file_force_download($file) {
        if (file_exists($file)) {
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
                ob_end_clean();
            }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            readfile($file);
            exit;
        }
    }

