<?php

    class Controller_index {

        private $Obj_model;

        public function __construct() {
            $this->Obj_model = new Model_index();
        }


        public function action_index(){
            $obj_view = new View('index/index', 'Test');

            $obj_view->books_range = $this->Obj_model->books_range();

            $obj_view->from            = (Input::isExistParam('from'))            ? Input::getParam('from') : '';
            $obj_view->to              = (Input::isExistParam('to'))              ? Input::getParam('to') : '';
            $obj_view->author          = (Input::isExistParam('author'))          ? Input::getParam('author') : '';
            $obj_view->number_of_books = (Input::isExistParam('number_of_books')) ? Input::getParam('number_of_books') : '';

            $obj_view->books_list = $this->Obj_model->get_books_list($obj_view->from, $obj_view->to, $obj_view->author, $obj_view->number_of_books);


            $obj_view->Show();
        }


        
    }