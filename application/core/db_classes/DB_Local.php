<?php

    //
    class DB_Local extends Database{

        //Singleton
        static public $_instance;
        public static function getInstance() {
            if( !(self::$_instance instanceof self) ){
                //echo 'IF Singleton<br>';
                self::$_instance = new self( Config::Get('LOCAL_DB') );
            }
            //echo 'ELSE Singleton<br>';
            return self::$_instance;
        }

    }