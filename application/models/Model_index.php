<?php

class Model_index {

    private $Obj_DB;


    public function __construct(){
        $this->Obj_DB  =  DB_Local::getInstance();
    }


    public function books_range(){
        $this->Obj_DB->Query("select  DATE_FORMAT((max(published)),'%Y') as max_publ, DATE_FORMAT((min(published)),'%Y') as min_publ  from `book`");
        if($this->Obj_DB->GetRows() > 0){
            return $this->Obj_DB->Fetch();
        }else{
            $now      = new DateTime;
            return array('min_publ' => 1970, 'max_publ' => $now->format('Y') );
        }
    }

    public function get_books_list($from, $to, $author, $number_of_books){

        $this->Obj_DB->Query("select A.*,  count(distinct B.isbn) as kol_isbn, group_concat(dIstinct B.isbn) as isbns, group_concat(dIstinct B.book_id) as book_ids from author A
            left join  author_book AB on A.author_id = AB.author_id
            left join book B on AB.book_id = B.book_id ".(empty($from) ? '' : "and B.published >='".$this->Obj_DB->QE($from)."'" )." ".(empty($to) ? '' : "and B.published <='".$this->Obj_DB->QE($to)."'" )."
            where 1 ".(empty($author) ? '' : "and (A.first_name like '%".$this->Obj_DB->QE($author)."%' or  A.surname like '%".$this->Obj_DB->QE($author)."%')"  )."
            group by A.author_id
            ".($number_of_books == '' ? '' : "having kol_isbn=".intval($number_of_books) )."
            ");
        return $this->Obj_DB->FetchAll();
    }

    /*
    public function check_email($email){
        $this->Obj_DB_Smarty->Query("select * from users where email = {0}", array($email));
        return $this->Obj_DB_Smarty->GetRows() > 0 ? true : false;
    }

*/

}