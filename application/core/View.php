<?php

    //класс Представление
    class View{
        protected $name_tpl;
        protected $title_tpl;
        protected $header_tpl;
        protected $footer_tpl;
        public $exception;

        public function __construct($name_tpl='index', $title_tpl='', $header_tpl='header', $footer_tpl='footer'){
            $this->name_tpl   = $name_tpl;
            $this->title_tpl  = $title_tpl;
            $this->header_tpl = $header_tpl;
            $this->footer_tpl = $footer_tpl;
            $this->exception = '';
        }

        //подключаем шаблон представления(view)
        public function Show(){
            //echo getcwd();
            //подключаем шапку
            $file_header = mb_strtolower($this->header_tpl);
            require_once ('views/headers_footers/' .$file_header. '.php');

            //подключаем контент
            $file_body = mb_strtolower($this->name_tpl);
            require_once ('views/' .$file_body. '.php');

            //подключаем подвал
            $file_footer = mb_strtolower($this->footer_tpl);
            require_once ('views/headers_footers/' .$file_footer. '.php');
        }

        // магические методы для динамического создание свойств. Что бы отслеживать адекватно ошибки.
        public function __get($name) {
            //возникновение ошибки
            try{
                if( isset($this->$name) ) {
                    return $this->$name;
                }
                else{
                    if( Config::Get('IS_LOCAL') ){
                        throw new Exception('Undefined property <strong style="color: red;"> '.$name.' </strong>referenced.');
                    }
                    else{
                        return '';
                    }
                }
            }catch( Exception $e ){
                //$this->exception .= '<hr/>DateTime : '.date('d-m-Y H:i:s', time())."<br/>";
                $this->exception .= '<pre>Message : '.$e->getMessage()."<br/>";
                //$this->exception .= 'TraceAsString : <pre>'.$e->getTraceAsString()."</pre>\n<br/>";
                $this->exception .= 'String : '.$e->__toString()."</pre>";

                //echo '<hr/>DateTime : ' .date('d-m-Y H:i:s', time()). '<br/>';
                //echo $e->getMessage() .'<br/>';
                //echo '<pre>' .$e->getTraceAsString(). '</pre>';
                //echo '<hr/>';

                return false;
            }
        }


        public function __set($name, $value) {
            $this->$name = $value;
            //$this->arr_properties[$name] = $value;
            //pre($this->arr_properties);
        }



    }