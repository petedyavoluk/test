<div class="container-fluid">
    <div class="row">
        <form class="form-inline">
            <div class="form-group">
                <label for="author">Author</label>
                <input type="text" class="form-control" id="author" placeholder="" value="<?=$this->author; ?>">
            </div>
            <div class="form-group">
                <label for="number_of_books">Number of books</label>
                <input type="text" class="form-control" id="number_of_books" placeholder="" value="<?=$this->number_of_books; ?>">
            </div>
            <div class="form-group">
                <label for="from">From</label>
                <input type="text" class="form-control mydatepicker" id="from" placeholder="" value="<?=$this->from; ?>">
            </div>
            <div class="form-group">
                <label for="to">To</label>
                <input type="text" class="form-control mydatepicker" id="to" placeholder="" value="<?=$this->to; ?>">
            </div>
            <button type="button" class="btn btn-default" id="Search">Search</button>
        </form>
    </div>
    <div class="row">
        <?php if($this->books_list === false) : ?>
            <p>No Search results;</p>
        <?php else : ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th> <th>Author</th> <th>number of books</th> <th>list of isbn</th> <th>list of book id</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    foreach($this->books_list as $one_book){
                        $i = 1;
                        echo '<tr><th scope="row">'.$i.'</th><td>'.$one_book['surname'].' '.$one_book['first_name'].'</td> <td>'.$one_book['kol_isbn'].'</td>
                         <td>'.$one_book['isbns'].'</td> <td>'.$one_book['book_ids'].'</td> </tr>';
                        $i++;
                    }
                ?>

                </tbody>
            </table>
        <?php endif; ?>

    </div>
</div>

<script>
    $( function() {
        $( ".mydatepicker" ).datepicker({
            dateFormat: "yy-mm-dd",
            yearRange: "<?=$this->books_range['min_publ'].':'.$this->books_range['max_publ'] ?>"
        });
    } );


    $(document).on("click", "#Search", function(){
        var author = $("#author").val();
        var number_of_books = $("#number_of_books").val();
        var from       = $("#from").val();
        var to         = $("#to").val();

        var url = '/index/index/';
        if(parseInt(number_of_books) >= 0 ){
            url+= 'number_of_books/' + number_of_books + '/';
        }
        if(from != ''){
            url+= 'from/' + from + '/';
        }
        if(to != ''){
            url+= 'to/' + to + '/';
        }
        if(author != ''){
            url+= 'author/' + author + '/';
        }
        window.location = url;

    });
</script>