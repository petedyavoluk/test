<?php

    //Класс для работы с базой
    abstract class Database{

        protected $mysqli; // обьект подключения
        protected $result; // результат query
        protected $id_row = -1; // Возвращает автоматически генерируемый ID, используя последний запрос
        protected $affected_rows = -1; // Получает число строк, затронутых предыдущей операцией
        protected $enable_logs = false; // включить логирование

        protected function __construct(array $connect, $enable_logs=false){
            $this->mysqli = new mysqli($connect['HOST'], $connect['USER'], $connect['PASSWORD'], $connect['DB_NAME']);
            // проверяем соединение
            if ($this->mysqli->connect_error) {
                $txt = "Error connect to ".$connect['HOST']." : \n" . $this->mysqli->connect_error;
                trigger_error($txt,E_USER_ERROR);
                ErrorOnPage("Oh no! Something went wrong. We're working hard to fix the problem. Stay tuned.");
                exit();
            }
            $this->enable_logs = $enable_logs;

            //установка параметров общения с базой по умолчанию
            $this->mysqli->query("SET NAMES 'utf8'");
            $this->mysqli->query("SET CHARACTER_SET_CLIENT='utf8'");
            $this->mysqli->query("SET CHARACTER_SET_RESULTS='utf8'");
            $this->mysqli->query("SET COLLATION_CONNECTION='utf8_general_ci'");


        }
        protected function __clone(){}

        public function __destruct(){
            //разрываем подключение к базе
            $this->mysqli->close();
        }

        //выполнение запроса
        public function Query( $query='', $params=array() , $debug=false){
            if( is_array( $params ) && count( $params ) > 0 ){
                foreach( $params as $k => $val ){
                    $query = str_replace( '{' . $k . '}', ( (substr($k, 0, 1) == 'f') ? ('`' .$this->mysqli->real_escape_string( $val ). '`') : ('\'' .$this->mysqli->real_escape_string( $val ). '\'') ), $query );
                }
            }

            //строка готового запроса
            if($debug){
                pre($query,'',1);
                return false;
            }

            //query
            if( ($this->result = $this->mysqli->query($query)) === false ){
                //throw new Exception( 'Syntax error in query: '. $query);
                if(Config::Get('IS_LOCAL')||_isAdmin()){
                    $text = "<pre>".$this->mysqli->error." \n<br/> Full query: ". $query."</pre>\n";
                    echo $text;
                    trigger_error($text,E_USER_ERROR);
                }else{
                    $text = '<p>SQL error on '.SITE_NAME.'</p>';
                    $text.= '<p>'.$this->mysqli->error.'</p>';
                    $text.= '<p>Full query: '.$query.'</p>';
                    _send_mail('petrd@smartyads.com', 'SQL error',$text);
                    trigger_error($text,E_USER_ERROR);
                }

                return false;
            }
            else{
                $this->id_row = $this->mysqli->insert_id; //ID
                $this->affected_rows = $this->mysqli->affected_rows; //count rows
                $query_lower  = strtolower($query);
                if($this->enable_logs && (strpos($query_lower, 'insert ')!==false || strpos($query_lower, 'update ')!==false || strpos($query_lower, 'delete ')!==false )){
                    $this->mysqli->query("insert into `update_log` SET `table_name`='-', `text_query`='".$this->mysqli->real_escape_string($query)."', `userid`=".(Auth::$_user!== false? Auth::$_user['id']: 0)." ");
                }

                return true;
            }
        }

        //получение количества вернувшихся строк
        public function GetRows(){
            return $this->affected_rows;
        }

        //извлекает результирующий ряд в виде ассоциативного массива
        public function Fetch(){
            /* извлечение ассоциативного массива */
            if($this->result !== false){
                return $this->result->fetch_assoc();
            }
            return false;
        }

        //получает все записи в асоциативном массиве
        public function FetchAll(){
            if($this->result !== false){
                if($this->affected_rows==0){
                    return false;
                }else{
                    $res = array();
                    while($row = $this->result->fetch_assoc()){
                        $res[] = $row;
                    }
                    return $res;
                }
            }
            return false;
        }

        //возвращает ID записи
        public function GetID(){
            return $this->id_row;
        }

        //вставка записей
        public function Insert( $table='', $params=array(), $ignore = '' ){
            $query = 'INSERT '.$ignore.' INTO `'.$table.'` SET ';
            foreach( $params as $k => $val ){
                $query.= '`' . $k . '`=' . ( is_null($val) ? "NULL" : ('\'' .$this->mysqli->real_escape_string( $val ). '\'') ). ', ';
            }
            $query =substr( $query, 0, -2 );

            //pre($query, '', 1);
            //query
            if( ($this->result = $this->mysqli->query($query)) === false ){
                //throw new Exception( 'Syntax error in query: '. $query);
                if(Config::Get('IS_LOCAL')||_isAdmin()){
                    echo "<pre>".$this->mysqli->error." \n<br/> Full query: ". $query."</pre>\n";
                }else{
                    $text = '<p>SQL error on '.Config::Get("SITE_NAME").'</p>';
                    $text.= '<p>'.$this->mysqli->error.'</p>';
                    $text.= '<p>Full query: '.$query.'</p>';
                    _send_mail('petrd@smartyads.com', 'SQL error',$text);
                    trigger_error($text,E_USER_ERROR);
                }
                return false;
            }
            else{
                $this->id_row = $this->mysqli->insert_id; //ID
                $this->affected_rows = $this->mysqli->affected_rows; //count rows

                if($this->enable_logs){
                    $this->mysqli->query("insert into `update_log` SET `table_name`='".$table."', `text_query`='".$this->mysqli->real_escape_string($query)."', `userid`=".(Auth::$_user!== false? Auth::$_user['id']: 0)." ");
                }

                return $this->id_row;
            }

        }

        //обновление записей
        public function Update( $table='', $params=array(), $where=array() ){
            $query = 'UPDATE `'.$table.'` SET ';
            foreach( $params as $k => $val ){
                $query.= '`' . $k . '`=' . ( is_null($val) ? "NULL" : ('\'' .$this->mysqli->real_escape_string( $val ). '\'') ). ', ';
            }
            $query =substr( $query, 0, -2 );
            $query.= ' WHERE 1=1';
            foreach( $where as $where_k => $where_val ){
                $query.= ' AND (`' . $where_k . '`=' . ('\'' .$this->mysqli->real_escape_string( $where_val ). '\'') . ')';
            }

            //query
            if( ($this->result = $this->mysqli->query($query)) === false ){
                //throw new Exception( 'Syntax error in query: '. $query);
                if(Config::Get('IS_LOCAL')||_isAdmin()){
                    echo "<pre>".$this->mysqli->error." \n<br/> Full query: ". $query."</pre>\n";
                }else{
                    $text = '<p>SQL error on '.Config::Get("SITE_NAME").'</p>';
                    $text.= '<p>'.$this->mysqli->error.'</p>';
                    $text.= '<p>Full query: '.$query.'</p>';
                    _send_mail('petrd@smartyads.com', 'SQL error',$text);
                    trigger_error($text,E_USER_ERROR);
                }
                return false;
            }
            else{
                $this->id_row = $this->mysqli->insert_id; //ID
                $this->affected_rows = $this->mysqli->affected_rows; //count rows
                if($this->enable_logs){
                    $this->mysqli->query("insert into `update_log` SET `table_name`='".$table."', `text_query`='".$this->mysqli->real_escape_string($query)."', `userid`=".(Auth::$_user!== false? Auth::$_user['id']: 0)." ");
                }

                return TRUE;
            }

        }

        public function Delete( $table='', $where=array() ){
            $query = 'DELETE FROM `'.$table.'`  ';
            $query.= ' WHERE 1=1';
            foreach( $where as $where_k => $where_val ){
                $query.= ' AND (`' . $where_k . '`=' . ('\'' .$this->mysqli->real_escape_string( $where_val ). '\'') . ')';
            }
            //query
            if( ($this->result = $this->mysqli->query($query)) === false ){
                //throw new Exception( 'Syntax error in query: '. $query);
                if(Config::Get('IS_LOCAL')||_isAdmin()){
                    echo "<pre>".$this->mysqli->error." \n<br/> Full query: ". $query."</pre>\n";
                }else{
                    $text = '<p>SQL error on '.Config::Get("SITE_NAME").'</p>';
                    $text.= '<p>'.$this->mysqli->error.'</p>';
                    $text.= '<p>Full query: '.$query.'</p>';
                    _send_mail('petrd@smartyads.com', 'SQL error',$text);
                }
                trigger_error($text,E_USER_ERROR);
                return false;
            }
            else{
                $this->id_row = $this->mysqli->insert_id; //ID
                $this->affected_rows = $this->mysqli->affected_rows; //count rows
                if($this->enable_logs){
                    $this->mysqli->query("insert into `update_log` SET `table_name`='".$table."', `text_query`='".$this->mysqli->real_escape_string($query)."', `userid`=".(Auth::$_user!== false? Auth::$_user['id']: 0)." ");
                }

                return TRUE;
            }

        }
        
        public function QE($str){
            return $this->mysqli->real_escape_string($str);
        }


    }