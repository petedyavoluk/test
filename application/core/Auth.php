<?php

    //
    class Auth{

        static public $_user = false;

        //проверка на залогиневание пользователя
        static public function Login(){
            if( self::$_user === false){
                if( isset($_COOKIE['cemail']) && isset($_COOKIE['cpassword']) ){
                    if (WLS) {
                        $obj_DB = DB_WL::getInstance();
                        $obj_DB->Query('SELECT * FROM `users` WHERE email = {0} AND active = 1 AND confirmterms=1 AND deletedon=0', array($_COOKIE['cemail']));
                    } else {
                        $obj_DB = DB_Smarty::getInstance();
                        $obj_DB->Query('SELECT * FROM `users` WHERE email = {0} AND deletedon = 0 AND banned = 0', array($_COOKIE['cemail']));
                    }
                    if($obj_DB->GetRows()>0){
                        $temp_user = $obj_DB->Fetch();
                        if( $_COOKIE['cpassword'] === md5($temp_user['salt'] . $temp_user['password']) ){
                            //продление cookie
                            $prolong_life_time = 3600;
                            if(isset($_COOKIE['rememberMe'])){
                                $prolong_life_time = 3600 * 24; //1 day
                            }
                            if (WLS) {
                                setcookie('cemail', $_COOKIE['cemail'], time() + $prolong_life_time, '/');
                                setcookie('cpassword', $_COOKIE['cpassword'], time() + $prolong_life_time, '/');
                            } else {
                                setcookie('cemail', $_COOKIE['cemail'], time() + $prolong_life_time, '/' , Config::Get('COOKIE_URL'));
                                setcookie('cpassword', $_COOKIE['cpassword'], time() + $prolong_life_time, '/', Config::Get('COOKIE_URL'));
                            }
                            self::$_user = $temp_user;
                        }
                    }
                }
            }
            return self::$_user;
        }

        //проверка на залогиневание пользователя через форму
        static public function LoginForm($email='', $password='', $remember_me=false){
            if( self::$_user === false){
                if( !empty($email) && !empty($password) ){
                    if (WLS) {
                        $obj_DB = DB_WL::getInstance();
                        $obj_DB->Query('SELECT * FROM `users` WHERE email = {0} AND active = 1 AND confirmterms=1 AND deletedon=0', array($email));
                    } else {
                        $obj_DB = DB_Smarty::getInstance();
                        $obj_DB->Query('SELECT * FROM `users` WHERE email = {0} AND deletedon = 0 AND banned = 0', array($email));
                    }
                    if($obj_DB->GetRows()>0){
                        $temp_user = $obj_DB->Fetch();
                        if( md5($password) ===$temp_user['password']){
                            //$salt = rand(1,9999);
                            $salt = $temp_user['salt'];
                            $obj_DB->Update('users', array('lastlogin' => time(),'logincount'=>$temp_user['logincount']+1), array('id'=>$temp_user['id']));
                            $cookie_password = md5($salt . $temp_user['password']) ;
                            //продление cookie
                            $prolong_life_time = 3600;
                            if($remember_me){
                                $prolong_life_time = 3600 * 24; //1 day
                                setcookie('rememberMe', 'one_day', time() + $prolong_life_time, '/' , Config::Get('COOKIE_URL'));
                            }
                            if (WLS) {
                                setcookie('cemail',$email, time() + $prolong_life_time, '/');
                                setcookie('cpassword',$cookie_password, time() + $prolong_life_time, '/');
                            } else {
                                setcookie('cemail', $email, time() + $prolong_life_time, '/', Config::Get('COOKIE_URL'));
                                setcookie('cpassword', $cookie_password, time() + $prolong_life_time, '/', Config::Get('COOKIE_URL'));
                            }
                            self::$_user = $temp_user;
                        }
                    }
                }
            }
            return self::$_user;
        }

        //Разрыв залогиненвания
        static public function Logout(){
            session_destroy();
            session_write_close();
            if (WLS) {
                setcookie('cemail', '', time() - 3600, '/');
                setcookie('cpassword', '', time() - 3600, '/');
                setcookie('rememberMe', '', time() - 3600, '/');
                setcookie(session_name(),'',0,'/');
            } else {
                setcookie('cemail', '', time() - 3600, '/', Config::Get('COOKIE_URL'));
                setcookie('cpassword', '', time() - 3600, '/', Config::Get('COOKIE_URL'));
                setcookie('rememberMe', '', time() - 3600, '/', Config::Get('COOKIE_URL'));
                setcookie(session_name(),'',0, Config::Get('COOKIE_URL'));
            }
            _Go(_Link('login'));
        }

    }