<?php

    //разбирает адресную строку и вытаскивает с неё имя котролера, действие, доп.параметры
    class Route{

        private function __construct(){}
        private function __clone(){}

        //обрабатывает запрос пришедший из строки браузера
        static public function Start(){

            //назначение параметров по умолчанию
            $controller_name = 'index'; //имя котролера по умолчанию
            $action_name = 'index'; //имя метода по умолчанию
            $action_parameters = array(); //доп.параметры в адрессной строке

            //преобразование строки запроса в элементы массива
            $request_uri = $_SERVER['REQUEST_URI'];
            $route_array = explode('/', trim($request_uri, '/'));

            //проверка на наличие 1-го элемента (имя класса)
            if( !empty($route_array[0]) ){
                $controller_name = mb_strtolower($route_array[0]);
            }
            //проверка на наличие 2-го элемента (имя метода)
            if( !empty($route_array[1]) ){
                $action_name = mb_strtolower($route_array[1]);
            }

            //проверка на наличие доп. параметров
            //ассоциативный массив параметров (четные-Название нечетные-значение)
            if( !empty($route_array[2]) ){
                $last_temp = false;
                $count_route_array = count($route_array);
                for($i = 2; $i < $count_route_array ; $i++ ){
                    if( !$last_temp ){
                        //ключь (название)
                        $last_temp = $route_array[$i];
                        //значение по умолчанию
                        $action_parameters[$last_temp] = '';
                    }
                    else{
                        //значение (параметр)
                        $action_parameters[$last_temp] = $route_array[$i];
                        //сброс ключа
                        $last_temp = false;
                    }
                }
            }
            Config::Set('current_controller', $controller_name );
            Config::Set('curent_action', $action_name );
            //добавляем префиксы (преобразуем к нижнему регистру)
            //$model_name      = 'Model_'      . mb_strtolower($controller_name);
            $controller_name = 'Controller_' . $controller_name;
            $action_name     = 'action_'     . $action_name;

/*
            Config::Set('logout_message', 'Your session has expired. Please log in again.');
            
            if($controller_name !== 'Controller_login'){
                if( !Auth::Login() ){
                    if(in_array($action_name, array('action_ajax', 'action_changeuser', 'action_changesrv')) ){
                        echo Config::Get('logout_message');
                        exit();
                    }
                    if(count($route_array) > 1){
                        _Go(_Link('login', 'index', 'from', implode('--', $route_array) ));
                    }
                    else{
                        _Go(_Link('login'));
                    }
                }
            }
*/
            //подключаем нужные файлы
            //Контролер
            $path_file = '';
            $path_file = 'controllers' . DIRECTORY_SEPARATOR . $controller_name . '.php';
            if( !file_exists($path_file) ){
                ErrorPage404();
            }
			
/*
            //проверка авторизации и роли пользователя
            if( Auth::Login()!==false ){
                    //_Go(_Link('login'));
                if(Auth::$_user['role']=='SA' ){
                    header('Location: '.SMARTY_SITE_URL);
                    exit();
                }elseif(!WLS && (Auth::$_user['role']=='DSP' || Auth::$_user['role']=='SSP')  ){
                    header('Location: '.'http://exchange'.Config::Get('COOKIE_URL'));
                    exit();
                }elseif(Auth::$_user['role']=='PUBL' ){
                    header('Location: '.'http://ssp'.Config::Get('COOKIE_URL'));
                    exit();
                }else{
                    if($controller_name == 'Controller_index'){
                        switch(Auth::$_user['role']){
                            case    'ADM':
                                _Go(_Link('bidder'));
                                break;
                            case    'DSP':
                                _Go(_Link('exchange', 'reportdsp'));
                                break;
                            case 'NGINAD':
                                if(Auth::$_user['is_banner']=='TRUE' || Auth::$_user['rtb_is_video']=='TRUE'){
                                    _Go(_Link('bidder'));
                                }else{
                                    ErrorPermission();
                                }
                                break;
                            case 'ADVERT':
                                _Go(_Link('bidder'));
                                break;
                            case    'SSP':
                                if (WLS)
                                    _Go(_Link('ssp', 'report'));
                                else
                                    _Go(_Link('exchange', 'reportssp'));
                                break;
                            default: ErrorPermission();
                        }
                    }
                }
            }
*/
            //создаём обьект полученного котролера ($controller_name - имя класса)
            $Obj_controller = new $controller_name();
            //pre($Obj_controller);

            //запускаем действие ($action_name - имя метода)
            //проверка на наличие метода
            if( method_exists($Obj_controller, $action_name ) ){
                Input::setAll( $action_parameters ); //слаживает доп параметры с адресной строки в массив параметров
                $Obj_controller->$action_name();
                //pre($action_name);
            }
            else{
                //запускаем метод по умолчанию
                //$Obj_controller->action_index();
                ErrorPage404();
            }
        }

    }