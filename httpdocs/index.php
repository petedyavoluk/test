<?php

    //test
    //включение буферизации
    //ob_start();

    //Включение вывода всех ошибок и предупреждений
    error_reporting(E_ALL);
    ini_set('display_errors', 2);
    ini_set('display_startup_errors', 1);

    //меняем рабочюю директорию
    chdir('../application');

    //подключаем фаил bootstrap
    require_once ('bootstrap.php');