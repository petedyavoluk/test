<?php

    //
    class Lang{

        static private $arr_vars = array();
        static public $current_lang;

        public static function setAll($action_parameters){
            self::$arr_vars = $action_parameters;
        }

        static public function Get( $v ){
            if(isset(self::$arr_vars[$v])){
                if(!empty(self::$arr_vars[$v][self::$current_lang])){
                    return self::$arr_vars[$v][self::$current_lang];
                }elseif(!empty(self::$arr_vars[$v]['en'])){
                    return self::$arr_vars[$v]['en'];
                }else{
                    //return false;
                    if( Config::Get('IS_LOCAL')){
                        return "<pre>empty translate for  Lang_key ( $v ) \n</pre>";
                    }else{
                        return $v;
                    }
                }
            }else{
                if( Config::Get('IS_LOCAL')){
                    return "<pre>No such Lang_key ( $v ) \n</pre>";
                }else{
                    return $v;
                }
            }
        }
    }