<?php

    //Автозагрузка нужных классов (spl_autoload_register)
    function mvc_autoload($class_name) {
        $file_path = $class_name. '.php';

        // core
        if( file_exists('core' .DIRECTORY_SEPARATOR. $file_path ) ){
            //echo "<srtrong>core/{$file_path}</srtrong><hr>";
            require_once ('core' .DIRECTORY_SEPARATOR. $file_path);
        }
        // controllers
        elseif( file_exists('controllers' .DIRECTORY_SEPARATOR. $file_path ) ){
            //echo "<srtrong>controllers/{$file_path}</srtrong><hr>";
            require_once ('controllers' .DIRECTORY_SEPARATOR. $file_path);
        }
        // models
        elseif( file_exists('models' .DIRECTORY_SEPARATOR. $file_path ) ){
            //echo "<srtrong>models/{$file_path}</srtrong><hr>";
            require_once ('models' .DIRECTORY_SEPARATOR. $file_path);
        }
        // core/DBclasses
        elseif( file_exists('core' .DIRECTORY_SEPARATOR. 'db_classes' .DIRECTORY_SEPARATOR. $file_path ) ){
            //echo "<srtrong>core/db_classes/{$file_path}</srtrong><hr>";
            require_once ('core' .DIRECTORY_SEPARATOR. 'db_classes' .DIRECTORY_SEPARATOR. $file_path);
        }
        // core/extra
        elseif( file_exists('core' .DIRECTORY_SEPARATOR. 'extra' .DIRECTORY_SEPARATOR. $file_path ) ){
            //echo "<srtrong>core/extra/{$file_path}</srtrong><hr>";
            require_once ('core' .DIRECTORY_SEPARATOR. 'extra' .DIRECTORY_SEPARATOR. $file_path);
        }
    }
    spl_autoload_register('mvc_autoload');

    //подключение файла вспомогательных функций
    require_once ('core' .DIRECTORY_SEPARATOR. '_helpers_functions.php');

    //фаил конфигураций
    require_once ('inc.php');

    //отслиживание оспользования памяти и времени работы скрипта
    //start_time_memory();

    //старт сессии или ее продление
    session_start();

    //langs
/*
    if (WLS)
        $obj_smarty_db = DB_WL::getInstance();
    else
        $obj_smarty_db = DB_Smarty::getInstance();
    $obj_smarty_db->Query('select * from multi_lang');
    $temp_array = array();
    if($obj_smarty_db->GetRows()>0){
        while($temp_row = $obj_smarty_db->Fetch()){
            $temp_array[$temp_row['lang_key']] = $temp_row;
        }
    }
    Lang::setAll($temp_array);
    unset($temp_array);
*/
    //запускаем маршрутизатор
    Route::Start();