<?php //Постраничная навигация

class Pagination {

        private $page;
        private $total;

        private $path;


        public function __construct() {

                $path = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

                if (!Input::getParam('page')) {
                        $this->path = $path;
                } else {
                        $this->path = preg_replace('/\/page\/\S+/', '', $path);
                }

        }


        public function countPage($count_elem_page, $count_elem_all) {

                // Извлекаем из URL текущую страницу
                @$this->page = Input::getParam('page');

                // Находим общее число страниц
                $this->total = (($count_elem_all - 1) / $count_elem_page) + 1;
                $this->total =  intval($this->total);

                // Определяем начало элементов для текущей страницы
                $this->page = intval($this->page);
                
                // Если значение $this->page меньше единицы или отрицательно
                // переходим на первую страницу
                // А если слишком большое, то переходим на последнюю
                if (empty($this->page) or $this->page < 0) $this->page = 1;
                if ($this->page > $this->total) $this->page = $this->total;
                
                // Вычисляем начиная с какого номера следует выводить элементы
                $start = $this->page * $count_elem_page - $count_elem_page;

                // Выбираем $count_elem_page элементов начиная с номера $start
                $result['start']           = $start;
                $result['count_elem_page'] = $count_elem_page;
                $result['count_elem_all']  = $count_elem_all;
                $result['page']            = $this->page;
                $result['total']           = $this->total;

                return $result;

        }


        public function printPages() {

                if ($this->total > 1) {

                        $begin = '<div class="report-paginator"><ul class="pagination">';

                        //Если чтраниц меньше или равно 7, то выводим все что есть
                        if ($this->total <= 7) {

                                $pages = '';
                                for ($page=1; $page<=$this->total; $page++) {
                                        if ($page == $this->page)
                                                $pages .= '<li class="active"><a href="' . $this->path . '/page/' . $page . '">' . $page . '</a></li>';
                                        else
                                                $pages .= '<li><a href="' . $this->path . '/page/' . $page . '">' . $page . '</a></li>';
                                }

                        } else {
                                // Проверяем нужна ли первая страница
                                if (($this->page - 1) >= 3) {
                                        $firstpage = '<li><a href="' . $this->path . '/page/1">1</a></li>';
                                } else {
                                        $firstpage = '';
                                }

                                // Проверяем нужно ли троеточие слева
                                if (($this->page - 1) > 3) {
                                        $pervpage = '<li class="disabled"><a href="javascript:void(0);">&hellip;</a></li>';
                                } else {
                                        $pervpage = '';
                                }

                                // Проверяем нужно ли троеточие справа
                                if (($this->total - $this->page) > 3) {
                                        $nextpage = '<li class="disabled"><a href="javascript:void(0);">&hellip;</a></li>';
                                } else {
                                        $nextpage = '';
                                }

                                // Проверяем нужна ли последняя страница
                                if (($this->total - $this->page) >= 3) {
                                        $lastpage = '<li><a href="' . $this->path . '/page/' . $this->total . '">' . $this->total . '</a></li>';
                                } else {
                                        $lastpage = '';
                                }


                                // Находим две ближайшие станицы с обоих краев, если они есть
                                if ($this->page - 2 > 0) $page2left = '<li><a href="' . $this->path . '/page/' . ($this->page - 2) . '">' . ($this->page - 2) . '</a></li>'; else $page2left = '';
                                if ($this->page - 1 > 0) $page1left = '<li><a href="' . $this->path . '/page/' . ($this->page - 1) . '">' . ($this->page - 1) . '</a></li>'; else $page1left = '';

                                if ($this->page + 1 <= $this->total) $page1right = '<li><a href="' . $this->path . '/page/' . ($this->page + 1) . '">' . ($this->page + 1) . '</a></li>'; else $page1right = '';
                                if ($this->page + 2 <= $this->total) $page2right = '<li><a href="' . $this->path . '/page/' . ($this->page + 2) . '">' . ($this->page + 2) . '</a></li>'; else $page2right = '';

                                $this->page = '<li class="active"><a href="' . $this->path . '/page/' . $this->page . '">' . $this->page . '</a></li>';

                                $pages = $firstpage . $pervpage . $page2left . $page1left . $this->page . $page1right . $page2right . $nextpage . $lastpage;

                        }

                        $end = "</ul></div>";

                        return $begin . $pages . $end;

                } elseif (Input::getParam('page')) {

                        _Go($this->path);

                }
        }


}